package ua.goit.offline4.controller;

import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.dao.OrderDAO;
import ua.goit.offline4.model.OrderCount;

import java.util.HashSet;
import java.util.Set;

public class OrderController {
    private OrderDAO orderDAO;

    @Transactional
    public void createOrder(){
        Set<OrderCount> allOrderCounts = new HashSet<>(orderDAO.findAll());
        OrderCount orderCount = new OrderCount();
        orderCount.setId(10);
        if (!allOrderCounts.contains(orderCount)){
            orderDAO.save(orderCount);
        }else System.out.println("OrderCount already exist");
    }
    @Transactional
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }
}
