package ua.goit.offline4.controller;

import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.dao.PizzaDAO;
import ua.goit.offline4.model.Pizza;

import java.util.HashSet;
import java.util.Set;

public class PizzaController {

    private PizzaDAO pizzaDAO;
    @Transactional
    public void createPizza(){
        Set<Pizza> allPizzas = new HashSet<>(pizzaDAO.findAll());
        Pizza pizza = new Pizza();
        pizza.setId(9);
        pizza.setName("Test");
        pizza.setComments("Test comment");
        if (!allPizzas.contains(pizza)){
            pizzaDAO.save(pizza);
        }else System.out.println("Pizza already exist");
    }

    public void setPizzaDAO(PizzaDAO pizzaDAO) {
        this.pizzaDAO = pizzaDAO;
    }
}
