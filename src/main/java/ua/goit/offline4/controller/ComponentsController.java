package ua.goit.offline4.controller;

import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.model.Components;
import ua.goit.offline4.dao.ComponentsDAO;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class ComponentsController {

    private ComponentsDAO componentsDAO;

    @Transactional
    public void createComponent(){

        Set<Components> allComponents = new HashSet<>(componentsDAO.findAll());
        Components components = new Components();
        components.setId(9);
        components.setName("Pineapples");
        components.setPrize(BigDecimal.valueOf(25));
        if (!allComponents.contains(components)){
            componentsDAO.save(components);
        }else System.out.println("Component already exist");
    }

    public void setComponentsDAO(ComponentsDAO componentsDAO) {
        this.componentsDAO = componentsDAO;
    }
}
