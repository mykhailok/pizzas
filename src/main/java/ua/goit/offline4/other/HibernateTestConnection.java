package ua.goit.offline4.other;



import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ua.goit.offline4.dao.hibernate.HibComponentsDAO;


public class HibernateTestConnection {

    private HibComponentsDAO componentsDAO;


    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        HibernateTestConnection main = context.getBean(HibernateTestConnection.class);
        main.start();

    }

    private void start() {

        System.out.println(componentsDAO.findAll());

    }

    public void setComponentsDAO(HibComponentsDAO componentsDAO) {
        this.componentsDAO = componentsDAO;
    }

    public HibComponentsDAO getComponentsDAO() {
        return componentsDAO;
    }
}
