package ua.goit.offline4.other;

import java.sql.*;

public class PingConnection {
    private static String URL = "jdbc:postgresql://localhost:5432/Offline";
    private static String user = "user";
    private static String password = "1234";

    public static void main(String[] args) {
        //-Dusername=XXX -Dpassword=YYY
        //System.getProperty("username"+"password"); -> Правый верхний угол на названии класа, Edit Configuration - VM options
        //System.getProperties(); -> получить все данные системы
        loadDriver();

        try (Connection connection = DriverManager.getConnection(URL, user, password);
                Statement statement = connection.createStatement()){
            System.out.println("Successfully connected to DB...");
            String sql = "SELECT * FROM pizzeria.components";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                System.out.println(resultSet.getString("name") + " - " + resultSet.getFloat("prize"));
                //System.out.println(resultSet.getFloat("prize"));
            }
        } catch (SQLException e) {
            System.out.println("Can't connect to DB...");
        }
    }

    private static void loadDriver() {
        try {
            System.out.println("Loading driver...");
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Driver not found");
            throw new RuntimeException(e);
        }
    }

}
