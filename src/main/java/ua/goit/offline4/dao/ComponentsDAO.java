package ua.goit.offline4.dao;

import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.model.Components;

import java.util.List;

public interface ComponentsDAO {
    List<Components> findAll();

    @Transactional
    void save (Components components);

    Components load(Long id);

    void remove (Components components);
}
