package ua.goit.offline4.dao;

import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.model.Pizza;

import java.util.List;

public interface PizzaDAO {
    List<Pizza> findAll();
    @Transactional
    void save(Pizza pizza);
}
