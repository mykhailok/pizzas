package ua.goit.offline4.dao;

import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.model.OrderCount;

import java.util.List;

public interface OrderDAO {

    List<OrderCount> findAll();

    @Transactional
    void save(OrderCount orderCount);
}
