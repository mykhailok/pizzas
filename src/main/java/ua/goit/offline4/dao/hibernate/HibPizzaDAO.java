package ua.goit.offline4.dao.hibernate;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.dao.PizzaDAO;
import ua.goit.offline4.model.Pizza;

import java.util.List;

public class HibPizzaDAO implements PizzaDAO {
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<Pizza> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select e from Pizza e").list();
    }

    @Override
    @Transactional
    public void save(Pizza pizza) {
        sessionFactory.getCurrentSession().save(pizza);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
