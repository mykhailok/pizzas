package ua.goit.offline4.dao.hibernate;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.dao.OrderDAO;
import ua.goit.offline4.model.OrderCount;

import java.util.List;

public class HibOrderDAO implements OrderDAO {
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<OrderCount> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from OrderCount").list();
    }

    @Override
    @Transactional
    public void save(OrderCount orderCount) {
        sessionFactory.getCurrentSession().save(orderCount);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
