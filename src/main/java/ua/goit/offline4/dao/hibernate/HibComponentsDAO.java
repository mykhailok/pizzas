package ua.goit.offline4.dao.hibernate;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ua.goit.offline4.model.Components;
import ua.goit.offline4.dao.ComponentsDAO;

import java.util.List;


public class HibComponentsDAO implements ComponentsDAO {
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<Components> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select e from Components e").list();
    }

    @Override
    @Transactional
    public void save(Components components) {
        sessionFactory.getCurrentSession().save(components);

    }

    @Override
    @Transactional
    public Components load(Long id) {
        Components result = sessionFactory.getCurrentSession().load(Components.class, id);
        if (result == null){
            throw new RuntimeException("Can't find Components by id " + id);
        }
        return result;
    }

    @Override
    @Transactional
    public void remove(Components components) {
        sessionFactory.getCurrentSession().delete(components);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}




