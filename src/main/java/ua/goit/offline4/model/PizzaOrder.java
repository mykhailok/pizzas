package ua.goit.offline4.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "pizza_order", schema = "pizzeria")
public class PizzaOrder implements Serializable {

    @Id
    @ManyToOne
    private Pizza pizza;

    @Id
    @ManyToOne
    private OrderCount order;

    private BigDecimal count;

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public OrderCount getOrderCount() {
        return order;
    }

    public void setOrderCount(OrderCount orderCount) {
        this.order = orderCount;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }
}
