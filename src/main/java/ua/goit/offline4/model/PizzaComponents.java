package ua.goit.offline4.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "pizza_components", schema = "pizzeria")
public class PizzaComponents implements Serializable{
    @Id
    @ManyToOne
    private Pizza pizza;
    @Id
    @ManyToOne
    private Components component;

    private BigDecimal amount;

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Components getComponents() {
        return component;
    }

    public void setComponents(Components components) {
        this.component = components;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
