package ua.goit.offline4.model;

import javax.persistence.*;

@Entity
@Table(name = "order", schema = "pizzeria")
public class OrderCount {
    @Id
    @Column(name = "id")
    private int id;

    public OrderCount() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderCount)) return false;

        OrderCount orderCount = (OrderCount) o;

        return id == orderCount.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
