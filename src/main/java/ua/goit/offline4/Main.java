package ua.goit.offline4;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ua.goit.offline4.controller.ComponentsController;
import ua.goit.offline4.controller.OrderController;
import ua.goit.offline4.controller.PizzaController;

public class Main {
    private ComponentsController componentsController;
    private OrderController orderController;
    private PizzaController pizzaController;

    public static void main(String[] args) {


        ApplicationContext applicationContext
                = new ClassPathXmlApplicationContext("app-context.xml", "hibernate-context.xml");

        Main main = applicationContext.getBean(Main.class);

        main.start();
    }

    private void start() {
        //componentsController.createComponent();
        orderController.createOrder();
    }

    public void setComponentsController(ComponentsController componentsController) {
        this.componentsController = componentsController;
    }

    public void setOrderController(OrderController orderController) {
        this.orderController = orderController;
    }

    public void setPizzaController(PizzaController pizzaController) {
        this.pizzaController = pizzaController;
    }
}

